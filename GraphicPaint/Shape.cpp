#include "Shape.h"


Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}

/*
	Function Prints the name and type of the shape
*/
void Shape::printDetails() const
{
	cout << this->_type.c_str() << " " << this->_name.c_str() << " ";
}


/*
	Function returns the type of the shape
*/
string Shape::getType() const
{
	return this->_type;
}

/*
Function returns the name of the shape
*/
string Shape::getName() const
{
	return this->_name;
}

