#include "Rectangle.h"

MyRectangle::MyRectangle(const Point& a, double length, double width, const string& type, const string& name)
	: Polygon(type, name)
{
	this->_points.push_back(Point(a)); // Upper left
	this->_points.push_back(Point(a) + Point(width, length)); //Lower Right
}

MyRectangle::~MyRectangle()
{
}

/*
	Returns the area of the rectangle
*/
double MyRectangle::getArea() const
{
	double w = abs(this->_points[0].getX() - this->_points[1].getX());
	double h = abs(this->_points[0].getY() - this->_points[1].getY());

	return w * h;
}

/*
	Returns the Perimeter of the rectangle
*/
double MyRectangle::getPerimeter() const
{
	double w = abs(this->_points[0].getX() - this->_points[1].getX());
	double h = abs(this->_points[0].getY() - this->_points[1].getY());
	
	return 2 * w + 2 * h;
}


/*
	Draws the rectangle
*/
void MyRectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}


/*
	Clears the rectangle
*/
void MyRectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


