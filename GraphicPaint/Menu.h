#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>

enum MenuChoice
{
	NONE = -1,
	ADD = 0,
	MOD = 1,
	DELALL = 2,
	EXIT = 3
};

class Menu
{
public:

	Menu();
	~Menu();
	void DrawAll();
	void DeleteAll();
	void AddShape();
	void ModShape();
	int MainMenu();
	void AddCircle();
	void AddArrow();
	void AddTriangle();
	void AddRectangle();
	void MoveShape(int idx);
	// more functions..

private: 

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
	vector<Shape*> _shapes;
};

