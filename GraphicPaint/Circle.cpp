#include "Circle.h"

Circle::Circle(const Point& center, double radius, const string& type, const string& name)
	: Shape(name, type)
{
	this->_center = Point(center);
	this->_radius = radius;
}

Circle::~Circle()
{
}

/*
	Returns the center point of the circle
*/
const Point& Circle::getCenter() const
{
	return this->_center;
}

/*
	returns the radius of the circle
*/
double Circle::getRadius() const
{
	return this->_radius;
}

/*
	returns the perimeter of the circle
*/
double Circle::getPerimeter() const
{
	return 2 * PI * this->_radius;
}

/*
	returns the area of the circle
*/
double Circle::getArea() const
{
	return PI * this->_radius * this->_radius;
}

/*
	Adds the given point values to the center of the circle
*/
void Circle::move(const Point& other)
{
	this->_center += other;
}

/*
	Prints the circle's details
*/
void Circle::printDetails() const
{
	Shape::printDetails();
	cout << "Perimeter : " << this->getPerimeter() << " Area: " << this->getArea() << " ";
}

/*
	Draws the circle
*/
void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = this->getCenter();
	board.draw_circle(c.getX(), c.getY(), this->getRadius(), BLUE, 100.0f).display(disp);	
}


/*
	Craws the circle
*/
void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), this->getRadius(), BLACK, 100.0f).display(disp);
}


