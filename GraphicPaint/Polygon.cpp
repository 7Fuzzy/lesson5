#include "Polygon.h"

Polygon::Polygon(const string& name, const string& type) : Shape(name, type)
{
}

Polygon::~Polygon()
{
}

/*
	Function adds the other point to every point of the shape 
*/
void Polygon::move(const Point & other)
{
	for (int i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] += other;
	}
}

/*
	Returns the polygons perimeter
*/
double Polygon::getPerimeter() const
{
	double p = 0;
	for (int i = 0; i < _points.size() - 1; i++)
	{
		p += _points[i].distance(_points[i + 1]);
	}

	return p;
}

/*
	Prints polygon details: name, type, area and perimeter
*/
void Polygon::printDetails() const
{
	Shape::printDetails();
	cout << "Perimeter: " << this->getPerimeter() << " Area: " << this->getArea() << " ";
}
