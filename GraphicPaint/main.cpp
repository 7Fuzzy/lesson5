#include "Menu.h"

#define ADD 0
#define MOD 1
#define DELALL 2
#define EXIT 3

void main()
{
	Menu m = Menu();
	int c = m.MainMenu();
	while (c != EXIT)
	{
		switch (c)
		{
		case ADD:
			m.AddShape();
			break;
		case MOD:
			m.ModShape();
			break;
		case DELALL:
			m.DeleteAll();
			break;
		case EXIT:
			break;
		default:
			cout << "\nInvalid Input. Press any Key to continue...";
			getchar();
			break;
		}
		c = m.MainMenu();
	}
}