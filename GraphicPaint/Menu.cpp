#include "Menu.h"

#define CIRCLE 0
#define ARROW 1
#define TRIANGLE 2
#define RECTANGLE 3

Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
	for (int i = 0; i < _shapes.size(); i++)
	{
		delete _shapes[i];
	}
	_shapes.clear();
}

/*
	Draws every shape in the shapes vector
*/
void Menu::DrawAll()
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->draw(*_disp, *_board);
	}
}

/*
	Clears and deletes every shape from the shapes vector
*/
void Menu::DeleteAll()
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[0]->clearDraw(*_disp, *_board);
		_shapes.erase(_shapes.begin());
	}
}

/*
	Main menu loop: prints the menu and returns the user's choice 
*/
int Menu::MainMenu()
{
	int choice = -1;
	cout << "0.) Add Shape" << endl;
	cout << "1.) Modify Shape" << endl;
	cout << "2.) Delete All Shapes" << endl;
	cout << "3.) Exit" << endl;
	cout << "Enter Your Choice: ";
	cin >> choice;
	getchar();
	system("cls");

	return choice;
}

/*
	Prints the add shape menu and adds the correct shape
*/
void Menu::AddShape()
{
	int choice = -1;
	while (choice < CIRCLE || choice > RECTANGLE)
	{
		cout << "0.) Add Circle" << endl;
		cout << "1.) Add Arrow" << endl;
		cout << "2.) Add Triangle" << endl;
		cout << "3.) Add Rectangle" << endl;
		cout << "Enter Your Choice: ";
		cin >> choice;
		getchar();
		cout << endl;
		switch (choice)
		{
		case CIRCLE:
			this->AddCircle();
			break;
		case ARROW:
			this->AddArrow();
			break;
		case TRIANGLE:
			this->AddTriangle();
			break;
		case RECTANGLE:
			this->AddRectangle();
			break;
		default:
			cout << "\nInvalid Choice, Press any key to continue...";
		}
		system("cls");
	}
	this->DrawAll();
}

/*
	Adds a circle
*/
void Menu::AddCircle()
{
	double x = 0;
	double y = 0;
    double r = 0;
	string name = "";

	cout << "Enter X: ";
	cin >> x;
	//cout << " X - " << x;
	getchar();
	cout << "Enter Y: ";
	cin >> y;
	//cout << " Y - " << y;
	Point center = Point(x, y);
	//cout << center.getX() << " " << center.getY();
	getchar();
	cout << "Enter Radius: ";
	cin >> r;
	getchar();
	cout << "Enter Name: ";
	cin >> name;
	getchar();

	Circle* c = new Circle(center, r, "Circle", name);
	_shapes.push_back(c);
	c->draw(*_disp, *_board);
}

/*
	Adds an arrow
*/
void Menu::AddArrow()
{
	double x1 = 0;
	double y1 = 0;
	double x2 = 0;
	double y2 = 0;
	string name = "";

	cout << "Enter X1: ";
	cin >> x1;
	cout << "Enter Y1: ";
	cin >> y1;
	cout << "Enter X2: ";
	cin >> x2;
	cout << "Enter Y2: ";
	cin >> y2;
	cout << "Enter Name :";
	cin >> name;
	this->_shapes.push_back(new Arrow(Point(x1, y1), Point(x2, y2), "Arrow", name));
}


/*
	Adds a triangle
*/

void Menu::AddTriangle()
{
	double x1 = 0;
	double y1 = 0;
	double x2 = 0;
	double y2 = 0;
	double x3 = 0;
	double y3 = 0;
	string name = "";

	cout << "Enter X1: ";
	cin >> x1;
	cout << "Enter Y1: ";
	cin >> y1;
	cout << "Enter X2: ";
	cin >> x2;
	cout << "Enter Y2: ";
	cin >> y2;
	cout << "Enter X3: ";
	cin >> x3;
	cout << "Enter Y3: ";
	cin >> y3;
	cout << "Enter Name :";
	cin >> name;
	getchar();
	if ((x1 == x2 && x2 == x3) || (y1 == y2 && y2 == y3)) // if its on the same axies
	{
		cout << "Coordinates are not valid, press any key to continue" << endl;
		getchar();
	}
	else
	{
		this->_shapes.push_back(new Triangle(Point(x1, y1), Point(x2, y2), Point(x3, y3), "Triangle", name));
	}
}

/*
	Adds a rectangle
*/
void Menu::AddRectangle()
{
	double x = 0;
	double y = 0;
	double w = 0;
	double h = 0;
	string name = "";

	cout << "Enter X: ";
	cin >> x;
	cout << "Enter Y: ";
	cin >> y;
	cout << "Enter Width: ";
	cin >> w;
	cout << "Enter Height: ";
	cin >> h;
	cout << "Enter Name: ";
	cin >> name;
	getchar();
	if (w == 0 || h == 0) // if the width or height are 0
	{
		cout << "Invalid width or height. Press any key to continue.";
		getchar();
	}
	else
	{
		this->_shapes.push_back(new MyRectangle(Point(x, y), h, w, "Rectangle", name));
	}
}

/*
	Opens the modify shape menu and handles user input
*/
void Menu::ModShape()
{
	int idx = -1;
	int choice = -1;
	if (_shapes.empty()) //no shapes?
	{
		cout << "No Shapes to modify. press any key " << endl;
		getchar();
		system("cls");
	}
	else
	{
		//Choose shape:
		while (idx < 0 || idx > _shapes.size())
		{
			//Prints all the shapes:
			for (int i = 0; i < _shapes.size(); i++)
			{
				cout << i << ".) for " << _shapes[i]->getName() << " (" << _shapes[i]->getType() << ")" << endl;
			}
			cout << "Enter Your Choice: ";
			cin >> idx;
			getchar();
			system("cls");
		}

		//Modify:
		while (choice < 0 || choice > 2)
		{
			cout << endl << "0.) Move Shape" << endl;
			cout << "1.) Get Details" << endl;
			cout << "2.) Remove Shape" << endl;
			cin >> choice;
			getchar();
			system("cls");
			switch (choice)
			{
			case 0: //Move
				MoveShape(idx);
				break;
			case 1: // Details
				cout << endl;
				_shapes[idx]->printDetails();
				getchar();
				break;
			case 2: //Delete
				_shapes[idx]->clearDraw(*_disp, *_board);
				_shapes.erase(_shapes.begin() + idx);
				break;
			default: //Invalid!
				cout << "Invalid choice. press any key to continue" << endl;
				getchar();
				break;
			}
		}
		system("cls");

	}
}

/*
	Moves the shape in the given place on the shapes vector
*/
void Menu::MoveShape(int idx)
{
	double x = 0;
	double y = 0;

	cout << "Enter Move X: ";
	cin >> x;
	getchar();
	cout << "Enter Move Y: ";
	cin >> y;
	getchar();

	_shapes[idx]->clearDraw(*_disp, *_board);
	_shapes[idx]->move(Point(x, y));
	DrawAll();
}