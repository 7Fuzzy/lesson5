#pragma once
#include "Polygon.h"



class MyRectangle : public Polygon
{
public:
	// There's a need only for the top left corner 
	MyRectangle(const Point& a, double length, double width, const string& type, const string& name);
	virtual ~MyRectangle();

	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual double getArea() const;
	virtual double getPerimeter() const;

};
