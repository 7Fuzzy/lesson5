#include "Arrow.h"

Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name) : Shape(name, type)
{
	this->_p1 = Point(a);
	this->_p2 = Point(b);
}

Arrow::~Arrow()
{
}

/*
	Adds the given point values to the arrow's edges
*/
void Arrow::move(const Point& other)
{
	this->_p1 += other;
	this->_p2 += other;
}

/*
	Draws the arrow
*/
void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}

/*
	Clears the arrow
*/
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}

double Arrow::getPerimeter() const
{
	return this->_p1.distance(this->_p2);
}

void Arrow::printDetails() const
{
	Shape::printDetails();
	cout << "Perimeter: " << this->getPerimeter() << " Area: 0 ";
}