#include "Point.h"

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point()
{
}

Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::~Point()
{
}

/*
	Function returns the X value of the point
*/
double Point::getX() const
{
	return _x;
}

/*
Function returns the Y value of the point
*/
double Point::getY() const
{
	return _y;
}


/*
	Operator adds values of given points and returns the new point created
*/
Point Point::operator+(const Point& other) const
{
	return Point(this->getX() + other.getX(), this->getY() + other.getY());
}

/*
	Operator adds the values of given point to the current one
*/
Point& Point::operator+=(const Point& other)
{
	this->_x += other.getX();
	this->_y += other.getY();
	return *this;
}


/*
	Function calculates the distance between the current and given points
*/
double Point::distance(const Point& other) const
{
	double num1 = this->_x - other._x;
	double num2 = this->_y - other._y;
	return sqrt(num1 * num1 + num2 * num2);  
}